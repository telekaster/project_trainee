const config = {
  setupFiles: ['./browserMocks.js'],
  testEnvironment: 'jsdom',
};

module.exports = config;
