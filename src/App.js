import React, { useEffect, lazy, Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { signInInputsProps, passwordInputsProps } from './pages/Auth/inputsProps.constants';
import { signInErrorHandler, forgotPasswordErrorHandler } from './service/validation';
import { getUserAction } from './pages/Auth/actions/user';
import { ROUTES } from './constants';
import { loadStudy } from './pages/MainPage/actions/study';
import { loadCourses, saveCombinedCourses } from './pages/MainPage/actions/courses';
import { filterCoursesByStudy } from './pages/MainPage/utils';
import { selectStudy, selectCourses } from './pages/MainPage/selectors';
import { createStructuredSelector } from 'reselect';
import ErrorBoundary from './service/ErrorBoundary';
import ErrorMessage from './components/ErrorMessage';
import PrivateRoute from './service/PrivateRoute';
import PublicRoute from './service/PublicRoute';
import Loader from './components/Loader';

const Header = lazy(() => import('./components/Header'));
const Auth = lazy(() => import('./pages/Auth'));
const MainPage = lazy(() => import('./pages/MainPage'));
const CoursePage = lazy(() => import('./pages/Course'));

export default function App() {
  const dispatch = useDispatch();

  const { study, courses } = useSelector(
    createStructuredSelector({
      study: selectStudy,
      courses: selectCourses,
    }),
    shallowEqual
  );
  const token = localStorage.getItem('AUTH_TOKEN');

  useEffect(() => {
    if (token) {
      dispatch(getUserAction());
      dispatch(loadStudy());
      dispatch(loadCourses());
    }
  }, [token]);

  useEffect(() => {
    dispatch(saveCombinedCourses(filterCoursesByStudy(study, courses)));
  }, [study, courses]);

  return (
    <Suspense fallback={<Loader global={true} />}>
      <Header />
      <Routes>
        <Route
          path={ROUTES.MAIN}
          element={
            <PrivateRoute>
              <ErrorBoundary>
                <MainPage />
              </ErrorBoundary>
            </PrivateRoute>
          }
        />
        <Route
          path={ROUTES.AUTH}
          element={
            <PublicRoute>
              <ErrorBoundary>
                <Auth
                  title={'Welcome Back'}
                  inputs={signInInputsProps}
                  errorHandler={signInErrorHandler}
                  buttonLabel="Sign in"
                />
              </ErrorBoundary>
            </PublicRoute>
          }
        />
        <Route
          path={ROUTES.FORGOT_PASS}
          element={
            <PublicRoute>
              <ErrorBoundary>
                <Auth
                  title={'Forgot your password?'}
                  inputs={passwordInputsProps}
                  errorHandler={forgotPasswordErrorHandler}
                  buttonLabel="Reset password"
                />
              </ErrorBoundary>
            </PublicRoute>
          }
        />
        <Route
          path={ROUTES.ARCHIVE}
          element={
            <PrivateRoute>
              <ErrorBoundary>
                <MainPage />
              </ErrorBoundary>
            </PrivateRoute>
          }
        />

        <Route
          path={ROUTES.COURSE}
          element={
            <PrivateRoute>
              <ErrorBoundary>
                <CoursePage />
              </ErrorBoundary>
            </PrivateRoute>
          }
        />
        <Route path="*" element={<ErrorMessage title={'Page not found'} />} />
      </Routes>
    </Suspense>
  );
}
