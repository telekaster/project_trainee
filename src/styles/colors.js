export const colors = {
  white: '#FFFFFF',
  lightGrey: '#B0B0B0',
  grey: '#949494',
  transparentGrey: 'rgba(58,58,58,0.5)',
  black: '#000000',
  lightBlack: '#1F1F1F',
};
