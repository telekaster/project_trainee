import axios from 'axios';

axios.defaults.baseURL = 'https://university-api.techstack.dev';
axios.defaults.headers.common['Authorization'] = localStorage.getItem('AUTH_TOKEN');

export default axios;
