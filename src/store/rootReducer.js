import { combineReducers } from 'redux';
import authReducer from '../pages/Auth/authReducer';
import educationReducer from '../pages/MainPage/educationReducer';

const rootReducer = combineReducers({
  auth: authReducer,
  education: educationReducer,
});

export default rootReducer;
