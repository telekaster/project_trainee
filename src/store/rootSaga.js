import loginSaga from '../pages/Auth/sagas/login';
import userSaga from '../pages/Auth/sagas/user';
import forgotPassSaga from '../pages/Auth/sagas/forgotPass';
import studySaga from '../pages/MainPage/sagas/study';
import coursesSaga from '../pages/MainPage/sagas/courses';
import { takeEvery } from '@redux-saga/core/effects';
import { LOGIN_ACTIONS, USER_ACTIONS, FORGOT_PASS_ACTIONS } from '../pages/Auth/actions/actionTypes';
import { STUDY_ACTIONS, COURSES_ACTIONS } from '../pages/MainPage/actions/actionTypes';

function* rootSaga() {
  yield takeEvery(LOGIN_ACTIONS.LOGIN, loginSaga);
  yield takeEvery(USER_ACTIONS.USER, userSaga);
  yield takeEvery(FORGOT_PASS_ACTIONS.FORGOT_PASS, forgotPassSaga);
  yield takeEvery(STUDY_ACTIONS.LOAD, studySaga);
  yield takeEvery(COURSES_ACTIONS.LOAD, coursesSaga);
}

export default rootSaga;
