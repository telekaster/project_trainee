import React, { useEffect } from 'react';
import StyledCoursePage from './StyledCoursePage';
import InfoArea from './components/infoArea';
import Chapter from './components/chapter/Chapter';
import Done from './svg/Done';
import ScrollWrapper from '../../components/ScrollWrapper';
import Loader from '../../components/Loader';
import { Container } from '../../styles/Container';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { getChapterNumber } from './components/chapter/utils';
import { useParams } from 'react-router-dom';
import { selectCombinedCourses } from '../MainPage/selectors';
import { sortChapters, filterCurrentCourse } from './utils';
import { loadStudy } from '../MainPage/actions/study';
import { saveCombinedCourses } from '../MainPage/actions/courses';
import { createStructuredSelector } from 'reselect';
import { selectStudy, selectCourses } from '../MainPage/selectors';
import { filterCoursesByStudy } from '../MainPage/utils';
import { getUserAction } from '../Auth/actions/user';
import { v4 as uuidv4 } from 'uuid';

export default function CardPage() {
  const dispatch = useDispatch();
  const params = useParams().id;
  const combinedCourses = useSelector(selectCombinedCourses);
  const currentCourse = filterCurrentCourse(combinedCourses, params);
  const chapters = sortChapters(currentCourse);
  const mentor = currentCourse?.admins[0]?.name;

  const { study, courses } = useSelector(
    createStructuredSelector({
      study: selectStudy,
      courses: selectCourses,
    }),
    shallowEqual
  );

  const setClassToChapters = (chapter, index, className, inProgress = true, status) => {
    const number = getChapterNumber(index);

    return (
      <li className={className} key={uuidv4()}>
        <div className={inProgress ? 'in-progress' : 'done'}>{inProgress ? number : <Done />}</div>
        <div className="chapter-area">
          <Chapter chapter={chapter} index={index} deadline={currentCourse?.deadline} status={status} mokka={params} />
        </div>
      </li>
    );
  };

  const checkChaptersStatus = (chapter, activeChapter, index) => {
    if (activeChapter.order - chapter.order > 1) {
      return setClassToChapters(chapter, index, 'chapter-item', false, 'Done');
    } else if (activeChapter.order - chapter.order === 1) {
      return setClassToChapters(chapter, index, 'chapter-item-last', false, 'In Progress');
    } else {
      return setClassToChapters(chapter, index, 'chapter-item-not-started', true, 'Not started');
    }
  };

  const renderChapters = () => {
    return (
      <Container>
        <ul className="chapters-list">
          {chapters?.map((chapter, index) => {
            return checkChaptersStatus(chapter, currentCourse?.activeChapter, index);
          })}
        </ul>
      </Container>
    );
  };

  useEffect(() => {
    dispatch(getUserAction());
    dispatch(loadStudy());
  }, []);

  useEffect(() => {
    dispatch(saveCombinedCourses(filterCoursesByStudy(study, courses)));
  }, [study, courses]);

  return (
    <StyledCoursePage>
      <div>
        <InfoArea
          averageMark={currentCourse?.averageMark}
          mentor={mentor}
          name={currentCourse?.name}
          technologies={currentCourse?.technologies}
        />

        <div className="content">
          {!currentCourse ? <Loader global={true} /> : <ScrollWrapper>{renderChapters()}</ScrollWrapper>}
        </div>
      </div>
    </StyledCoursePage>
  );
}
