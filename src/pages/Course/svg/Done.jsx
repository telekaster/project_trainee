import React from 'react';

export default function Done() {
  return (
    <svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16 1.42076L5.89749 12L0 5.82422L1.28241 4.40345L5.89749 9.23631L14.7176 0L16 1.42076Z"
        fill="white"
      />
    </svg>
  );
}
