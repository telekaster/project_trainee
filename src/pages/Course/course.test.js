import { sortChapters, filterCurrentCourse } from "./utils";

describe("course_utils", () => {
  test("sortChapters", () => {
    const testingObj = {
      chapters: [{ order: 2 }, { order: 4 }, { order: 3 }, { order: 1 }],
    };
    const result = [{ order: 1 }, { order: 2 }, { order: 3 }, { order: 4 }];

    expect(sortChapters(testingObj)).toEqual(result);
  });

  test("filterCurrentCourse", () => {
    const testingArr = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
    const searchParams = "4";
    const result = { id: 4 };

    expect(filterCurrentCourse(testingArr, searchParams)).toEqual(result);
  });
});
