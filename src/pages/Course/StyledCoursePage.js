import styled from 'styled-components';
import { colors } from '../../styles/colors';
import { fonts } from '../../styles/fonts';

const StyledCoursePage = styled.div`
  .content {
    padding-bottom: 40px;
    height: 100vh;
    width: 100%;
    min-width: 1440px;
    background: ${colors.grey};
  }

  .chapters-list {
    padding-top: 81px;
    margin-left: 125px;
  }

  .chapter-item {
    position: relative;
    margin-bottom: 16px;
    min-height: 130px;
    height: 100%;
    border-left: 3px solid ${colors.lightBlack};
  }

  .chapter-item-last {
    position: relative;
    margin-bottom: 16px;
    min-height: 130px;
    height: 100%;
    border-left: 3px solid ${colors.white};
  }

  .chapter-item-not-started {
    position: relative;
  }
  .chapter-area {
    margin-bottom: 16px;

    position: relative;
    transform: translate(87px, -56px);
  }
  .done {
    width: 36px;
    height: 36px;
    background: ${colors.lightBlack};
    border-radius: 50%;

    position: absolute;
    transform: translate(-20px, -16px);

    display: flex;
    justify-content: center;
    align-items: center;
  }

  .in-progress {
    width: 36px;
    height: 36px;
    background: ${colors.white};
    border-radius: 50%;

    position: absolute;
    transform: translate(-17px, -15px);

    display: flex;
    justify-content: center;
    align-items: center;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.12;
    color: ${colors.lightBlack};
  }
`;

export default StyledCoursePage;
