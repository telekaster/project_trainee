export const getChapterMark = marks => {
  let sum = 0;

  if (marks) {
    marks.map(mark => {
      sum += mark.mark;
    });

    return sum / marks.length;
  }
};

export const findMark = (marks, mentorId) => {
  let result;

  if (marks) {
    marks.map(mark => {
      if (mark.initiatorId === mentorId) {
        result = mark.mark;
      }
    });

    return result;
  }
};

export const getChapterNumber = index => {
  return index + 1;
};

export const convertDeadlineDate = deadline => {
  const date = new Date(deadline);

  let day = date.getDate().toString();

  if (day.length === 1) {
    day = 0 + day;
  }

  let month = date.getMonth().toString();

  if (month.length === 1) {
    month = 0 + month;
  }

  let year = date.getFullYear();

  return {
    day,
    month,
    year,
  };
};
