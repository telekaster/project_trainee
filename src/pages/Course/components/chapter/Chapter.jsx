import React, { useState } from 'react';
import StyledChapter from './StyledChapter';
import ArrowDown from './svg/ArrowDown';
import ArrowUp from './svg/ArrowUp';
import { getChapterMark, findMark, getChapterNumber, convertDeadlineDate } from './utils';
import { v4 as uuidv4 } from 'uuid';

export default function Chapter({ chapter, index = 0, deadline, status }) {
  const [cardDeployed, setCardDeployed] = useState(false);
  const { name, comments, marks } = chapter;
  const { day, month, year } = convertDeadlineDate(deadline);

  const deployHandler = () => {
    setCardDeployed(deployed => !deployed);
  };

  const renderNotDeployedPart = () => {
    return (
      <>
        <div className="top-area">
          <h3 className="title">{name}</h3>
          <div className="top-area-right">
            {!isNaN(day, month, year) && (
              <p className="deadline">
                <span className="deadline-title">Deadline: </span>
                {day}.{month}.{year}
              </p>
            )}
            <div className="mark">{getChapterMark(marks) || '-'}</div>
          </div>
        </div>
        <p className="chapter-number">
          Chapter <span>{getChapterNumber(index)}</span>: {status}
        </p>
      </>
    );
  };

  const renderDeployedPart = () => {
    return (
      <>
        {!comments || comments.length === 0 ? (
          <p className="no-comments">No comments</p>
        ) : (
          <ul className="comments-list">
            {comments.map(comment => {
              return (
                <li key={uuidv4()}>
                  <div className="comments-item">
                    <div className="mentor-area">
                      Mentor: <p className="text">{comment.initiatorName}</p>
                    </div>
                    <div className="comment-area">
                      Comment: <p className="text">{comment.comment}</p>
                    </div>
                    <div className="mark-area">
                      Points: <p className="text">{findMark(marks, comment.initiatorId) || '-'}</p>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        )}

        <button type="button" className="arrow_up" onClick={deployHandler}>
          <ArrowUp />
        </button>
      </>
    );
  };

  return !chapter ? (
    'Loading'
  ) : (
    <StyledChapter>
      {renderNotDeployedPart()}
      {!cardDeployed ? (
        <button type="button" className="arrow_down" onClick={deployHandler}>
          <ArrowDown />
        </button>
      ) : (
        renderDeployedPart()
      )}
    </StyledChapter>
  );
}
