import styled from 'styled-components';
import { colors } from '../../../../styles/colors';
import { fonts } from '../../../../styles/fonts';

const StyledChapter = styled.div`
  min-width: 924px;
  background: ${colors.white};
  border-radius: 12px;

  .top-area {
    padding-top: 24px;
    margin-bottom: 8px;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .title {
    margin-left: 24px;

    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.12;
    color: ${colors.lightBlack};
  }

  .top-area-right {
    display: flex;
  }

  .deadline {
    margin-right: 24px;

    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.12;
    color: ${colors.lightBlack};

    .deadline-title {
      color: #b0b0b0;
    }
  }

  .mark {
    margin-right: 24px;

    width: 36px;
    height: 36px;
    border-radius: 50%;
    background: ${colors.lightBlack};

    display: flex;
    justify-content: center;
    align-items: center;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.12;
    color: ${colors.white};
  }

  .chapter-number {
    margin-left: 24px;
    padding-bottom: 64px;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightBlack};
  }

  .arrow_down {
    position: absolute;
    transform: translate(27px, -47px);

    background: none;
    border: none;

    cursor: pointer;
  }

  .comments-list {
    animation-duration: 0.5s;
    animation-name: appearence;
  }

  @keyframes appearence {
    from {
      opacity: 0;
    }

    to {
      opacity: 1;
    }
  }

  .comments-item {
    padding-top: 16px;
    padding-bottom: 16px;
    border-top: 1px solid ${colors.lightBlack};

    display: flex;
  }

  .comments-item-last {
    padding-top: 16px;
    padding-bottom: 16px;
    margin-bottom: 64px;

    border-top: 1px solid ${colors.lightBlack};
    border-bottom: 1px solid ${colors.lightBlack};

    display: flex;
  }

  .mentor-area {
    margin-left: 24px;
    margin-right: 133px;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightBlack};

    .text {
      font-weight: 700;
    }
  }

  .comment-area {
    width: 527px;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightBlack};
    text-overflow: ellipsis;

    .text {
      width: 527px;
      font-weight: 700;
    }
  }

  .no-comments {
    margin-left: 24px;
    margin-bottom: 27px;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: red;

    animation-duration: 0.5s;
    animation-name: appearence;
  }
  .mark-area {
    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightBlack};

    .text {
      font-weight: 700;
    }
  }

  .arrow_up {
    margin-left: 27px;
    margin-top: 64px;
    margin-bottom: 27px;

    background: none;
    border: none;

    cursor: pointer;
  }
`;

export default StyledChapter;
