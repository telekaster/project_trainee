import React from 'react';

export default function ArrowDown() {
  return (
    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M10.0337 -3.4822e-07L10.0337 15.6432L7.96638 15.6432L7.96638 -4.38584e-07L10.0337 -3.4822e-07Z"
        fill="#1F1F1F"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18 9.49998L9 18L-4.15257e-07 9.49998L1.46179 8.1194L9 15.2388L16.5382 8.1194L18 9.49998Z"
        fill="#1F1F1F"
      />
    </svg>
  );
}
