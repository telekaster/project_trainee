import React from 'react';

export default function ArrowUp() {
  return (
    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M7.96635 18L7.96635 2.35675L10.0336 2.35675L10.0336 18L7.96635 18Z" fill="#1F1F1F" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.13286e-07 8.50002L9 1.07324e-07L18 8.50002L16.5382 9.8806L9 2.76116L1.46179 9.8806L1.13286e-07 8.50002Z"
        fill="#1F1F1F"
      />
    </svg>
  );
}
