import React from 'react';
import StyledInfoArea from './StyledInfoArea';
import { Container } from '../../../../styles/Container';
import { useNavigate } from 'react-router-dom';
import WhiteArrow from './svg/WhiteArrow';
import { ROUTES } from '../../../../constants';

export default function InfoArea({ averageMark, mentor = '-', name, technologies }) {
  const navigate = useNavigate();

  const goBack = () => {
    if (window.history.length < 3) {
      navigate(ROUTES.MAIN);
    } else {
      navigate(-1);
    }
  };

  return (
    <StyledInfoArea>
      <Container>
        <div className="course-name-area">
          <div className="course-title">
            <button className="back-button" onClick={goBack}>
              <WhiteArrow />
            </button>
            <h2 className="title">{name}</h2>
          </div>
          <div className="course-mark">{typeof averageMark === 'number' ? averageMark.toFixed(1) : '-'}</div>
        </div>
        <div className="technology-area">
          {technologies && (
            <ul className="technology-list">
              {technologies.map(tech => {
                return (
                  <li className="technology-item" key={tech.id}>
                    {tech.name}
                  </li>
                );
              })}
            </ul>
          )}

          <div className="mentor-area">
            <p>
              Mentor: <span className="mentor-name">{mentor}</span>
            </p>
          </div>
        </div>
      </Container>
    </StyledInfoArea>
  );
}
