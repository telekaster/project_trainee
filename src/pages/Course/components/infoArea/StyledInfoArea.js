import styled from 'styled-components';
import { colors } from '../../../../styles/colors';
import { fonts } from '../../../../styles/fonts';

const StyledInfoArea = styled.div`
  min-width: 1440px;
  width: 100%;

  background: ${colors.lightBlack};

  .back-button {
    background: none;
    border: none;
    cursor: pointer;
  }
  .course-name-area {
    padding-top: 81px;
    margin-bottom: 32px;

    display: flex;
    align-items: center;
  }

  .title {
    margin-left: 11px;
    margin-right: 8px;

    display: inline;

    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.12;
    color: ${colors.white};
  }

  .course-mark {
    width: 36px;
    height: 36px;
    background: ${colors.white};
    border-radius: 50%;

    display: flex;
    justify-content: center;
    align-items: center;

    font-family: ${fonts.mainFont};

    font-size: 16px;
    line-height: 1.12;
    /* identical to box height, or 112% */

    color: #1f1f1f;
  }
  .technology-area {
    padding-bottom: 24px;

    display: flex;
    align-items: center;
  }
  .technology-list {
    display: flex;
  }

  .technology-item {
    padding: 12px;
    background: ${colors.white};
    border-radius: 6px;

    display: flex;
    justify-content: center;
    align-items: center;

    font-family: ${fonts.mainFont};

    font-size: 20px;
    line-height: 0.8;
    color: ${colors.lightBlack};
  }

  .technology-item:not(:last-child) {
    margin-right: 8px;
  }

  .mentor-area {
    margin-left: auto;
    margin-right: 200px;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.white};
  }

  .mentor-name {
    display: block;
  }
`;

export default StyledInfoArea;
