import React from 'react';

export default function WhiteArrow() {
  return (
    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M18 10.0337L2.35675 10.0337L2.35675 7.96638L18 7.96638L18 10.0337Z" fill="white" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.50002 18L-7.86805e-07 9L8.50002 8.30514e-07L9.8806 1.46179L2.76116 9L9.8806 16.5382L8.50002 18Z"
        fill="white"
      />
    </svg>
  );
}
