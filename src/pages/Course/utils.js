export const sortChapters = courses => {
  const chapters = courses?.chapters?.sort((a, b) => {
    return a.order - b.order;
  });

  return chapters;
};

export const filterCurrentCourse = (combinedCourses, params) => {
  if (combinedCourses.length) {
    let currentCourse;

    const numberedParams = Number(params);

    combinedCourses.map(course => {
      if (course.id === numberedParams) {
        currentCourse = course;
      }
    });

    if (currentCourse) {
      return currentCourse;
    } else {
      throw new Error('Page not found');
    }
  }
};
