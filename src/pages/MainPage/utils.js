export const filterCoursesByStudy = (study, courses) => {
  const combinedArray = [];

  study?.map(st => {
    courses?.map(course => {
      if (st.courseId === course.id && st.courseId === 1) {
        const mokkaComments = {
          id: st.id,
          activeChapter: st.activeChapter,
          admins: st.admins,
          closetAt: st.closedAt,
          deadline: st.deadline,
          averageMark: st.status.averageMark,
          name: course.name,
          technologies: course.technologies,
          chapters: [
            ...st.chapters,
            {
              id: 1,
              name: 'Intro Course 2',
              order: 1,
              marks: [{ mark: 4 }],
              comments: [
                {
                  initiatorName: 'Semeon Zaltz',
                  comment: 'Good work',
                },
                {
                  initiatorName: 'Semeon Zaltz',
                  comment: '-',
                },
                {
                  initiatorName: 'Semeon Zaltz',
                  comment: 'Nice work!',
                },
              ],
              pullRequests: null,
            },
          ],
        };

        combinedArray.push(mokkaComments);
      } else if (st.courseId === course.id) {
        const combinedObject = {
          id: st.id,
          activeChapter: st.activeChapter,
          admins: st.admins,
          chapters: st.chapters,
          closetAt: st.closedAt,
          deadline: st.deadline,
          averageMark: st.status.averageMark,
          name: course.name,
          technologies: course.technologies,
        };

        combinedArray.push(combinedObject);
      }
    });
  });

  return combinedArray;
};

export const getStudyByProcess = (courses, error) => {
  const available = [];
  const archive = [];

  if (courses && !error?.hasError) {
    courses.map(item => {
      if (!item.closetAt) {
        available.push(item);
      } else {
        archive.push(item);
      }
    });
  }

  return {
    available,
    archive,
  };
};

export const searchHandler = (setSearchArr, courseArray, searchValue) => {
  setSearchArr([]);

  const result = [];

  courseArray.map(item => {
    if (item?.name.toLowerCase().includes(searchValue)) {
      result.push(item);
    }

    setSearchArr(result);
  });
};
