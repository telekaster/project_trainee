import { STUDY_ACTIONS, COURSES_ACTIONS, COMBINED_COURSES_ACTIONS } from './actions/actionTypes';

const INITIAL_STATE = {
  studyProcesses: [],
  studyProcessesError: {},
  courses: [],
  coursesError: {},
  combinedCourses: [],
  loadingStudy: false,
  loadingCourses: false,
};

const educationReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case STUDY_ACTIONS.LOAD:
      return {
        ...state,
        loadingStudy: true,
      };

    case STUDY_ACTIONS.SUCCESS:
      return {
        ...state,
        studyProcesses: action.payload,
        studyProcessesError: {},
        loadingStudy: false,
      };

    case STUDY_ACTIONS.FAIL:
      return {
        ...state,
        studyProcessesError: action.payload,
        loadingStudy: false,
      };

    case COURSES_ACTIONS.LOAD:
      return {
        ...state,
        loadingCourses: true,
      };

    case COURSES_ACTIONS.SUCCESS:
      return {
        ...state,
        courses: action.payload,
        loadingCourses: false,
      };

    case COURSES_ACTIONS.FAIL:
      return {
        ...state,
        coursesError: action.payload,
        loadingCourses: false,
      };

    case COMBINED_COURSES_ACTIONS.SAVE:
      return {
        ...state,
        combinedCourses: action.payload,
      };

    default:
      return state;
  }
};

export default educationReducer;
