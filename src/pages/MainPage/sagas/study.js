import { put, call } from '@redux-saga/core/effects';
import { loadStudySuccess, loadStudyFail } from '../actions/study';
import { getStudy } from '../api/getStudy';

function* studySaga() {
  const response = yield call(getStudy);

  const data = response.data;

  if (response.hasError) {
    yield put(loadStudyFail(response));
  } else if (!data) {
    const noData = {
      hasError: true,
      status: '',
      message: 'No data',
      data: null,
    };

    yield put(loadStudyFail(noData));
  } else {
    yield put(loadStudySuccess(data));
  }
}

export default studySaga;
