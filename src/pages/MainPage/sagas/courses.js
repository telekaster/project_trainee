import { put, call } from '@redux-saga/core/effects';
import { getCourses } from '../api/getCourses';
import { coursesFail, coursesSuccess } from '../actions/courses';

function* coursesSaga() {
  const response = yield call(getCourses);

  const data = response.data;

  if (response.hasError) {
    yield put(coursesFail(response));
  } else if (!data) {
    const noData = {
      hasError: true,
      status: '',
      message: 'No data',
      data: null,
    };

    yield put(coursesFail(noData));
  } else {
    yield put(coursesSuccess(data));
  }
}

export default coursesSaga;
