import React, { useEffect, useState } from 'react';
import CourseCard from './components/courseCard';
import ScrollWrapper from '../../components/ScrollWrapper';
import ToolsBar from './components/ToolsBar';
import Loader from '../../components/Loader';
import { StyledMainPage } from './StyledMainPage';
import { Container } from '../../styles/Container';
import { useSelector, shallowEqual } from 'react-redux';
import { searchErrorHandler } from '../../service/validation';
import { getStudyByProcess, searchHandler } from './utils';
import { useLocation } from 'react-router-dom';
import { ROUTES } from '../../constants';
import { createStructuredSelector } from 'reselect';
import { selectStudyError, selectCombinedCourses, selectloadingStudy, selectloadingCourses } from './selectors';

export default function MainPage() {
  const [searchValue, setSearchValue] = useState('');
  const [searchError, setSearchError] = useState(null);
  const [searchArr, setSearchArr] = useState([]);
  const currentLocation = useLocation().pathname;

  const { error, combinedCourses, loadingStudy, loadingCourses } = useSelector(
    createStructuredSelector({
      error: selectStudyError,
      combinedCourses: selectCombinedCourses,
      loadingStudy: selectloadingStudy,
      loadingCourses: selectloadingCourses,
    }),
    shallowEqual
  );
  const { available, archive } = getStudyByProcess(combinedCourses, error);

  const renderSearchedCard = () => {
    return !searchArr.length ? (
      <Container>
        <p className="no-courses">Not found</p>
      </Container>
    ) : (
      <Container>
        <ul className="content-list">
          {searchArr.map(item => {
            return (
              <li className="content-item" key={item.id}>
                <CourseCard
                  name={item.name}
                  id={item.id}
                  mark={item.averageMark}
                  mentor={item.admins[0].name}
                  next={item.activeChapter.name}
                  technologies={item.technologies}
                />
              </li>
            );
          })}
        </ul>
      </Container>
    );
  };

  const renderCard = courses => {
    return !courses.length === 0 ? (
      <Container>
        <p className="no-courses">You have no courses</p>
      </Container>
    ) : (
      <Container>
        <ul className="content-list">
          {courses.map(item => {
            return (
              <li className="content-item" key={item.id}>
                <CourseCard
                  name={item.name}
                  id={item.id}
                  mark={item.averageMark}
                  mentor={item.admins[0].name}
                  next={item.activeChapter.name}
                  technologies={item.technologies}
                />
              </li>
            );
          })}
        </ul>
      </Container>
    );
  };

  const renderAllCards = () => {
    if (currentLocation === ROUTES.MAIN) {
      return renderCard(available);
    }

    if (currentLocation === ROUTES.ARCHIVE) {
      return renderCard(archive);
    }
  };

  useEffect(() => {
    if (currentLocation === ROUTES.MAIN) {
      searchHandler(setSearchArr, available, searchValue);
    } else {
      searchHandler(setSearchArr, archive, searchValue);
    }

    setSearchError(searchErrorHandler(searchValue));

    if (!searchValue) {
      setSearchArr([]);
    }
  }, [searchValue]);

  return (
    <StyledMainPage>
      <div className="tools-area">
        <Container>
          <ToolsBar
            available={available.length}
            archive={archive.length}
            error={searchError}
            setSearchValue={setSearchValue}
            searchHandler={searchHandler}
          />
        </Container>
      </div>

      <div className="content-area">
        {loadingStudy || loadingCourses ? (
          <Loader global={true} />
        ) : (
          <ScrollWrapper>{searchValue ? renderSearchedCard() : renderAllCards()}</ScrollWrapper>
        )}
      </div>
    </StyledMainPage>
  );
}
