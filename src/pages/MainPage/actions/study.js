import { STUDY_ACTIONS } from './actionTypes';

export const loadStudy = () => ({ type: STUDY_ACTIONS.LOAD });

export const loadStudySuccess = data => ({
  type: STUDY_ACTIONS.SUCCESS,
  payload: data,
});

export const loadStudyFail = data => ({
  type: STUDY_ACTIONS.FAIL,
  payload: data,
});
