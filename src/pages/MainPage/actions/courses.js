import { COURSES_ACTIONS, COMBINED_COURSES_ACTIONS } from './actionTypes';

export const loadCourses = data => ({
  type: COURSES_ACTIONS.LOAD,
  payload: data,
});

export const coursesSuccess = data => ({
  type: COURSES_ACTIONS.SUCCESS,
  payload: data,
});

export const coursesFail = data => ({
  type: COURSES_ACTIONS.FAIL,
  payload: data,
});

export const saveCombinedCourses = data => ({
  type: COMBINED_COURSES_ACTIONS.SAVE,
  payload: data,
});
