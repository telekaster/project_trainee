const DEFAULT_STATE = {
  ARRAY: [],
  OBJECT: {},
  BOOLEAN: false,
};

export const selectStudy = state => state.education.studyProcesses || DEFAULT_STATE.ARRAY;
export const selectStudyError = state => state.education.studyProcessesError || DEFAULT_STATE.OBJECT;
export const selectCourses = state => state.education.courses || DEFAULT_STATE.ARRAY;
export const selectCombinedCourses = state => state.education.combinedCourses || DEFAULT_STATE.ARRAY;
export const selectloadingStudy = state => state.education.loadingStudy || DEFAULT_STATE.BOOLEAN;
export const selectloadingCourses = state => state.education.loadingCourses || DEFAULT_STATE.BOOLEAN;
