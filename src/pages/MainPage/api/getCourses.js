import axios from '../../../api/axios.config';

export async function getCourses() {
  const response = await axios
    .get('/courses')
    .then(response => ({
      hasError: false,
      status: response.status,
      message: response.data.message,
      data: response.data.data,
    }))
    .catch(error => ({
      hasError: true,
      status: error.response.status,
      message: error.response.data.message,
      data: null,
    }));

  return response;
}
