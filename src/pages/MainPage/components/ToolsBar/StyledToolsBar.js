import styled from 'styled-components';
import { fonts } from '../../../../styles/fonts';
import { colors } from '../../../../styles/colors';

const StyledToolsBar = styled.div`
  padding-top: 75px;
  display: flex;
  justify-content: space-between;

  .mainPage-links-list {
    display: flex;
  }

  .mainPage-links-item {
    margin-right: 65px;
  }
  .mainPage-navlink {
    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.13;
    color: ${colors.lightGrey};
    .count {
      position: absolute;
      transform: translateX(10px);

      font-size: 16px;
      line-height: 1.25;
    }
  }

  .mainPage-navlink-active {
    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.13;
    color: ${colors.white};
    .count {
      position: absolute;
      transform: translateX(10px);
      font-size: 16px;
      line-height: 1.25;
    }
  }
  .mainPage-navlink-active::after {
    display: block;
    height: 4px;
    margin-top: 20px;
    background-color: #fff;
    content: '';
  }

  .mainPage-search-input {
    min-width: 397px;
    height: 44px;

    background: inherit;
    border: none;
    border-bottom: 1px solid ${colors.lightGrey};

    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.13;
    color: ${colors.lightGrey};
  }
  .mainPage-search-input::placeholder {
    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.13;
    color: ${colors.lightGrey};
  }
  .mainPage-search-input-error-message {
    font-family: ${colors.mainFont};
    font-size: 18px;
    line-height: 1.11;
    color: red;

    position: absolute;
    transform: translateY(-25px);
  }
`;

export default StyledToolsBar;
