import React from 'react';
import StyledToolsBar from './StyledToolsBar';
import { NavLink } from 'react-router-dom';
import { ROUTES } from '../../../../constants';
import { setActiveLink, debounce } from './utils';

export default function ToolsBar({ available, archive, error, setSearchValue }) {
  function onChange(e) {
    setSearchValue(e.target.value.toLowerCase());
  }

  const searchHandler = debounce(onChange, 500);

  return (
    <StyledToolsBar>
      <ul className="mainPage-links-list">
        <li className="mainPage-links-item">
          <NavLink to={ROUTES.MAIN} className={({ isActive }) => setActiveLink(isActive)}>
            Available course
            <span className="count">{available}</span>
          </NavLink>
        </li>
        <li>
          <NavLink to={ROUTES.ARCHIVE} className={({ isActive }) => setActiveLink(isActive)}>
            Archive
            <span className="count">{archive}</span>
          </NavLink>
        </li>
      </ul>
      <div>
        {error && <p className="mainPage-search-input-error-message">{error.message}</p>}

        <input
          type="text"
          className="mainPage-search-input"
          placeholder="Search for the course"
          onChange={searchHandler}
        />
      </div>
    </StyledToolsBar>
  );
}
