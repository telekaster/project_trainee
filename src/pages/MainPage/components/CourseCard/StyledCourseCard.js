import styled from 'styled-components';
import { colors } from '../../../../styles/colors';
import { fonts } from '../../../../styles/fonts';

const StyledCourseCard = styled.div`
  margin: auto;
  max-width: 1030px;
  background: ${colors.white};
  border-radius: 12px;
  @keyframes appears {
    from {
      transform: scale(0.1);
    }

    to {
      transform: scale(1);
    }
  }
  animation: appears 0.2s linear;

  .courseCard-container {
    padding-top: 24px;
    margin-right: 24px;
    padding-bottom: 24px;
    margin-left: 24px;

    border-bottom: 1px solid #1f1f1f;
  }
  .courseCard-primary {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .courseCard-primary-title {
    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.13;
    color: ${colors.lightBlack};
  }

  .courseCard-primary-mark-zone {
    width: 36px;
    height: 36px;
    background: ${colors.lightBlack};
    border-radius: 50%;

    display: flex;
    align-items: center;
    justify-content: center;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.12;
    color: ${colors.white};
  }

  .courseCard-secondary-title {
    padding-top: 8px;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightBlack};
  }

  .courseCard-technology-area {
    margin-top: 16px;

    display: flex;
    justify-content: space-between;
  }

  .courseCard-technology-list {
    display: flex;
  }

  .courseCard-technology-item {
    background: #1f1f1f;
    border-radius: 6px;
    padding: 8px 12px 8px 10px;

    font-family: ${fonts.mainFont};
    font-size: 20px;
    line-height: 1.25;
    color: ${colors.white};
  }
  .courseCard-technology-item:not(:last-child) {
    margin-right: 8px;
  }

  .courseCard-technology-mentor {
    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightBlack};
  }

  .courseCard-technology-mentor-name {
    font-family: ${fonts.mainFont};
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25;
    color: ${colors.lightBlack};
  }

  .courseCart-next-area {
    padding-top: 21px;
    margin-right: 24px;
    padding-bottom: 18px;
    margin-left: 24px;

    display: flex;
    justify-content: flex-end;
    align-items: center;
  }

  .courseCart-next-link {
    margin-right: 11px;
    font-family: ${fonts.mainFont};
    font-size: 32px;
    line-height: 1.12;
    color: ${colors.black};
    .courseCart-next-title {
      color: #b0b0b0;
    }
  }
  .courseCart-next-link-arrow {
    margin-left: 11px;
  }
`;

export default StyledCourseCard;
