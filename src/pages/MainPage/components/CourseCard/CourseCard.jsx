import React from 'react';
import StyledCourseCard from './StyledCourseCard';
import Arrow from './svg/Arrow';
import { Link } from 'react-router-dom';

export default function CourseCard({ id, mark, mentor, next, name, technologies }) {
  return (
    <StyledCourseCard>
      <div className="courseCard-container">
        <div className="courseCard-primary">
          <h2 className="courseCard-primary-title">{name}</h2>
          <div className="courseCard-primary-mark-zone">{mark ? mark.toFixed(1) : '-'}</div>
        </div>
        <p className="courseCard-secondary-title">{name}</p>

        <div className="courseCard-technology-area">
          <ul className="courseCard-technology-list">
            {technologies &&
              technologies.map(item => {
                return (
                  <li className="courseCard-technology-item" key={item.id}>
                    {item.name}
                  </li>
                );
              })}
          </ul>
          <div>
            <p className="courseCard-technology-mentor">Mentor:</p>
            <p className="courseCard-technology-mentor-name">{mentor}</p>
          </div>
        </div>
      </div>

      <div className="courseCart-next-area">
        <Link to={`/course/${id}`} className="courseCart-next-link">
          <span className="courseCart-next-title">Next: </span>
          {next}
          <span className="courseCart-next-link-arrow">
            <Arrow />
          </span>
        </Link>
      </div>
    </StyledCourseCard>
  );
}
