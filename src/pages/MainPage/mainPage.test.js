import { getStudyByProcess } from "./utils";

describe("MainPage_utils", () => {
  const courses = [
    {
      id: 3,
      closetAt: "2022-01-10T09:00:00.000Z",
    },
    {
      id: 4,
      closetAt: null,
    },
  ];

  const result = {
    available: [
      {
        id: 4,
        closetAt: null,
      },
    ],
    archive: [
      {
        id: 3,
        closetAt: "2022-01-10T09:00:00.000Z",
      },
    ],
  };

  test("filterCoursesByStudy", () => {
    expect(getStudyByProcess(courses)).toEqual(result);
  });
});
