import styled from 'styled-components';
import { fonts } from '../../styles/fonts';
import { colors } from '../../styles/colors';

export const StyledMainPage = styled.div`
  .tools-area {
    min-width: 1440px;
    width: 100%;
    background-color: ${colors.lightBlack};
  }

  .content-area {
    min-width: 1440px;
    width: 100%;
    height: 100vh;
    background: ${colors.lightGrey};
  }

  .content-item {
    padding-bottom: 16px;
  }
  .content-item:first-child {
    padding-top: 24px;
  }

  .loader {
    position: absolute;
    top: 50%;
    left: 50%;
  }

  .no-courses {
    padding-top: 24px;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightBlack};
  }
`;
