import { LOGIN_ACTIONS, USER_ACTIONS, FORGOT_PASS_ACTIONS, LOGOUT_ACTIONS } from './actions/actionTypes';

const LOGIN_INITIAL_STATE = {
  token: '',
  tokenError: '',
  user: {},
  userError: {},
  forgotPass: '',
  forgotPassError: '',
  loggedIn: false,
  loadingLogin: false,
  loadingUser: false,
};

const authReducer = (state = LOGIN_INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_ACTIONS.LOGIN:
      return {
        ...state,
        loadingLogin: true,
      };

    case LOGIN_ACTIONS.SUCCESS:
      return {
        ...state,
        token: action.payload,
        tokenError: '',
        forgotPass: '',
        forgotPassError: '',
        loadingLogin: false,
      };

    case LOGIN_ACTIONS.FAIL:
      return {
        ...state,
        tokenError: action.payload,
        loadingLogin: false,
      };

    case USER_ACTIONS.USER:
      return {
        ...state,
        loadingLogin: true,
        loadingUser: true,
      };

    case USER_ACTIONS.SUCCESS:
      return {
        ...state,
        user: action.payload,
        userError: {},
        loggedIn: true,
        loadingLogin: false,
        loadingUser: false,
      };

    case USER_ACTIONS.FAIL:
      return {
        ...state,
        userError: action.payload,
        user: {},
        loggedIn: false,
        loadingLogin: false,
        loadingUser: false,
      };

    case FORGOT_PASS_ACTIONS.FORGOT_PASS:
      return {
        ...state,
        loadingUser: true,
      };

    case FORGOT_PASS_ACTIONS.SUCCESS:
      return {
        ...state,
        forgotPass: action.payload,
        forgotPassError: '',
        tokenError: '',
        loadingUser: false,
      };

    case FORGOT_PASS_ACTIONS.FAIL:
      return {
        ...state,
        forgotPassError: action.payload,
        tokenError: '',
        loadingUser: false,
      };

    case LOGOUT_ACTIONS.LOGOUT:
      return {
        ...state,
        loggedIn: false,
        token: '',
        user: {},
      };

    default:
      return state;
  }
};

export default authReducer;
