import styled from 'styled-components';
import { fonts } from '../../styles/fonts';
import { colors } from '../../styles/colors';

export const MainArea = styled.div`
  background: ${colors.lightBlack};
  height: 100%;
  width: 100%;
  min-width: 1440px;

  .content {
    height: 100vh;
    display: flex;
    align-items: center;
  }
  .title {
    margin-bottom: 48px;
    font-family: ${fonts.mainFont};
    font-size: 48px;
    line-height: 1.17;
    color: ${colors.white};
  }
  .forgot-password-link {
    display: block;
    margin-top: 24px;
    margin-bottom: 48px;
    max-width: 171px;

    font-family: ${fonts.mainFont};
    font-size: 18px;
    line-height: 1.22;
    color: ${colors.white};
  }
  .error-message-submit {
    font-family: ${fonts.mainFont};
    font-size: 18px;
    line-height: 1.11;
    color: red;
    min-width: 300px;

    position: absolute;
    transform: translate(100px, -35px);
  }

  .success-message {
    font-family: ${fonts.mainFont};
    font-size: 18px;
    line-height: 1.11;
    color: green;
    min-width: 300px;

    position: absolute;
    transform: translate(0px, -35px);
  }
`;
