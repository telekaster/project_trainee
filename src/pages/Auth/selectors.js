const DEFAULT_STATE = {
  STRING: ``,
  OBJECT: {},
  BOOLEAN: false,
};

export const selectTokenError = state => state.auth.tokenError || DEFAULT_STATE.STRING;
export const selectResetPassword = state => state.auth.forgotPass || DEFAULT_STATE.STRING;
export const selectResetPasswordError = state => state.auth.forgotPassError || DEFAULT_STATE.STRING;
export const selectLoadingLogin = state => state.auth.loadingLogin || DEFAULT_STATE.BOOLEAN;
export const selectLoadingUser = state => state.auth.loadingUser || DEFAULT_STATE.BOOLEAN;
