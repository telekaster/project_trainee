import axios from '../../../api/axios.config';

export async function getUser() {
  axios.defaults.headers.common['Authorization'] = localStorage.getItem('AUTH_TOKEN');

  const response = await axios
    .get('/users/myself')
    .then(response => ({
      hasError: false,
      status: response.status,
      message: response.data.message,
      data: response.data.data,
    }))
    .catch(error => ({
      hasError: true,
      status: error.response.status,
      message: error.response.data,
      data: null,
    }));

  return response;
}
