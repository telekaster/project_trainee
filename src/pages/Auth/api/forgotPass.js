import axios from '../../../api/axios.config';

export async function forgotPass(payload) {
  const user = {};

  for (const key in payload) {
    user[key.toLowerCase()] = payload[key];
  }

  const response = await axios
    .post('/authorization/reset-password', user)
    .then(response => ({
      hasError: false,
      status: response.status,
      message: response.data.message,
      data: response.data.data,
    }))
    .catch(error => ({
      hasError: true,
      status: error.response.status,
      message: error.response.data.message,
      data: null,
    }));

  return response;
}
