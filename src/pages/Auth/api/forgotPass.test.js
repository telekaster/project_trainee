import axios from "axios";

jest.mock("axios");

test("Forgot password request", async () => {
  const request = { email: "my3@mail.com" };
  const response = { data: "_uj49H4H8O" };

  axios.post.mockReturnValue(response);

  const data = await axios.post("/authorization/reset-password", request);

  expect(axios.post).toBeCalledTimes(1);
  expect(data).toEqual(response);
});
