import { FORGOT_PASS_ACTIONS } from './actionTypes';

export const forgotPassAction = data => ({
  type: FORGOT_PASS_ACTIONS.FORGOT_PASS,
  payload: data,
});

export const forgotPassSuccessAction = data => ({
  type: FORGOT_PASS_ACTIONS.SUCCESS,
  payload: data,
});

export const forgotPassFailAction = data => ({
  type: FORGOT_PASS_ACTIONS.FAIL,
  payload: data,
});
