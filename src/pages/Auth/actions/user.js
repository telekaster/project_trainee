import { USER_ACTIONS } from './actionTypes';

export const getUserAction = () => ({ type: USER_ACTIONS.USER });

export const getUserActionSuccess = data => ({
  type: USER_ACTIONS.SUCCESS,
  payload: data,
});

export const getUserActionFail = data => ({
  type: USER_ACTIONS.FAIL,
  payload: data,
});
