import { LOGIN_ACTIONS } from './actionTypes';

export const loginAction = data => ({
  type: LOGIN_ACTIONS.LOGIN,
  payload: data,
});

export const loginSuccessAction = data => ({
  type: LOGIN_ACTIONS.SUCCESS,
  payload: data,
});

export const loginFailAction = data => ({
  type: LOGIN_ACTIONS.FAIL,
  payload: data,
});
