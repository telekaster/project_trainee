export const LOGIN_ACTIONS = {
  LOGIN: 'LOGIN',
  SUCCESS: 'LOGIN_SUCCESS',
  FAIL: 'LOGIN_FAIL',
};

export const USER_ACTIONS = {
  USER: 'GET_MY_USER',
  SUCCESS: 'GET_MY_USER_SUCCESS',
  FAIL: 'GET_MY_USER_FAIL',
};

export const FORGOT_PASS_ACTIONS = {
  FORGOT_PASS: 'FORGOT_PASS',
  SUCCESS: 'FORGOT_PASS_SUCCESS',
  FAIL: 'FORGOT_PASS_FAIL',
};

export const LOGOUT_ACTIONS = { LOGOUT: 'LOGOUT' };
