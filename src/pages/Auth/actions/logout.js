import { LOGOUT_ACTIONS } from './actionTypes';

export const logoutAction = () => ({ type: LOGOUT_ACTIONS.LOGOUT });
