import React, { useEffect } from 'react';
import Input from '../../components/Input';
import Arrow from '../../components/Button/svg/Arrow';
import Button from '../../components/Button';
import uniqid from 'uniqid';
import Loader from '../../components/Loader';
import { Container } from '../../styles/Container';
import { MainArea } from './StyledAuth';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { loginAction } from './actions/login';
import { forgotPassAction } from './actions/forgotPass';
import { ROUTES } from '../../constants';
import {
  selectTokenError,
  selectResetPassword,
  selectResetPasswordError,
  selectLoadingLogin,
  selectLoadingUser,
} from './selectors';
import { createStructuredSelector } from 'reselect';

export default function Auth({ title, inputs, buttonLabel, errorHandler }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation().pathname;

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm();

  const { tokenError, resetPassword, resetPasswordError, loadingLogin, loadingUser } = useSelector(
    createStructuredSelector({
      tokenError: selectTokenError,
      resetPassword: selectResetPassword,
      resetPasswordError: selectResetPasswordError,
      loadingLogin: selectLoadingLogin,
      loadingUser: selectLoadingUser,
    }),
    shallowEqual
  );

  const renderInputs = inputsProps => {
    return inputsProps?.map(props => {
      return (
        <Input
          {...props}
          key={uniqid()}
          register={register}
          error={errors}
          setError={setError}
          authError={tokenError}
        />
      );
    });
  };

  const submit = formData => {
    const message = errorHandler(formData);

    if (message?.error) {
      setError(message.name, { message: message.message });
    }

    if (!message?.error) {
      if (location === ROUTES.AUTH) {
        dispatch(loginAction(formData));
      } else {
        dispatch(forgotPassAction(formData));
      }
    }
  };

  useEffect(() => {
    if (resetPassword) {
      navigate(ROUTES.AUTH);
    }
  }, [resetPassword]);

  const renderLoader = () => {
    return loadingLogin || loadingUser ? <Loader global={false} /> : <Arrow />;
  };

  return (
    <MainArea>
      <Container>
        <div className="content">
          <div>
            <h2 className="title">{title}</h2>

            <form onSubmit={handleSubmit(submit)}>
              {renderInputs(inputs)}
              {location === ROUTES.AUTH && (
                <Link to={ROUTES.FORGOT_PASS} className="forgot-password-link">
                  Forgot the password?
                </Link>
              )}
              {tokenError && <p className="error-message-submit">The email or password is incorrect</p>}
              {resetPassword && (
                <p className="success-message">Your new password was send to you. Please check your e-mail</p>
              )}
              {resetPasswordError && <p className="error-message-submit">The email is incorrect</p>}
              <Button type="submit" text={buttonLabel} ButtonIcon={renderLoader()} handler={() => {}} />
            </form>
          </div>
        </div>
      </Container>
    </MainArea>
  );
}
