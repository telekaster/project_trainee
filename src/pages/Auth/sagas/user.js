import { put, call } from '@redux-saga/core/effects';
import { getUser } from '../api/user';
import { getUserActionSuccess, getUserActionFail } from '../actions/user';

function* userSaga() {
  const response = yield call(getUser);

  if (response.hasError) {
    const error = response.message;

    yield put(getUserActionFail(error));
    yield localStorage.removeItem('AUTH_TOKEN');
  } else {
    const auth = { ...response.data };

    yield put(getUserActionSuccess(auth));
  }
}

export default userSaga;
