import { put, call } from '@redux-saga/core/effects';
import { loginSuccessAction, loginFailAction } from '../actions/login';
import { getUserAction } from '../actions/user';
import { login } from '../api/login';
import axios from 'axios';
import { loadStudy } from '../../MainPage/actions/study';
import { loadCourses } from '../../MainPage/actions/courses';

function* loginSaga(action) {
  const response = yield call(login, action.payload);

  if (response.hasError) {
    yield put(loginFailAction(response.message));
  } else {
    const data = response.data;

    yield put(loginSuccessAction(data));
    localStorage.setItem('AUTH_TOKEN', data);
    const token = localStorage.getItem('AUTH_TOKEN');

    axios.defaults.headers.common['Authorization'] = token;

    yield put(getUserAction());
    yield put(loadStudy());
    yield put(loadCourses());
  }
}

export default loginSaga;
