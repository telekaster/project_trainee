import loginSaga from "./login";
import forgotPassSaga from "./forgotPass";
import userSaga from "./user";
import { login } from "../api/login";
import { forgotPass } from "../api/forgotPass";
import { getUser } from "../api/user";
import { put, call } from "@redux-saga/core/effects";
import {
  LOGIN_ACTIONS,
  FORGOT_PASS_ACTIONS,
  USER_ACTIONS,
} from "../actions/actionTypes";

describe("Auth_sagas", () => {
  const user = { Email: "my3@mail.com", Password: "_uAF?V)18O" };
  const action = { payload: user };
  const responseSuccess = {
    hasError: false,
  };
  const responseFail = {
    hasError: true,
  };

  test("Login_success", () => {
    const saga = loginSaga(action);
    expect(saga.next().value).toEqual(call(login, user));

    expect(saga.next(responseSuccess).value).toEqual(
      put({ type: LOGIN_ACTIONS.SUCCESS, payload: responseSuccess.data })
    );
  });

  test("Login_fail", () => {
    const saga = loginSaga(action);
    expect(saga.next().value).toEqual(call(login, user));

    expect(saga.next(responseFail).value).toEqual(
      put({ type: LOGIN_ACTIONS.FAIL, payload: responseFail.data })
    );
  });

  test("Reset_pass_success", () => {
    const saga = forgotPassSaga(action);
    expect(saga.next().value).toEqual(call(forgotPass, user));

    expect(saga.next(responseSuccess).value).toEqual(
      put({ type: FORGOT_PASS_ACTIONS.SUCCESS, payload: responseSuccess.data })
    );
  });

  test("Reset_pass_fail", () => {
    const saga = forgotPassSaga(action);
    expect(saga.next().value).toEqual(call(forgotPass, user));

    expect(saga.next(responseFail).value).toEqual(
      put({
        type: FORGOT_PASS_ACTIONS.FAIL,
        payload: responseFail,
      })
    );
  });

  test("Current_user_success", () => {
    const saga = userSaga(action);
    expect(saga.next().value).toEqual(call(getUser));

    expect(saga.next(responseSuccess).value).toEqual(
      put({
        type: USER_ACTIONS.SUCCESS,
        payload: { ...responseSuccess.data },
      })
    );
  });

  test("Current_user_fail", () => {
    const saga = userSaga(action);
    expect(saga.next().value).toEqual(call(getUser));

    expect(saga.next(responseFail).value).toEqual(
      put({
        type: USER_ACTIONS.FAIL,
        payload: responseSuccess.message,
      })
    );
  });
});
