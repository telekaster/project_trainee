import { put, call } from '@redux-saga/core/effects';
import { forgotPassSuccessAction, forgotPassFailAction } from '../actions/forgotPass';
import { forgotPass } from '../api/forgotPass';

function* forgotPassSaga(action) {
  const response = yield call(forgotPass, action.payload);

  if (response.hasError) {
    yield put(forgotPassFailAction(response));
  } else {
    const data = response.data;

    yield put(forgotPassSuccessAction(data));
  }
}

export default forgotPassSaga;
