import errorMessages from '../components/Input/inputsErrorMessages';

function emailValidation(data, pattern, forbidenSymbols) {
  if (!data.Email) {
    return {
      error: true,
      name: 'Email',
      message: errorMessages().required,
    };
  } else if (data.Email.length > 50) {
    return {
      error: true,
      name: 'Email',
      message: errorMessages('Email', 50).maxLength,
    };
  } else if (!pattern.test(data.Email)) {
    return {
      error: true,
      name: 'Email',
      message: errorMessages().emailPattern,
    };
  } else if (forbidenSymbols.test(data.Email)) {
    return {
      error: true,
      name: 'Email',
      message: errorMessages().forbidenSymbols,
    };
  } else {
    return null;
  }
}

function passwordValidation(data, pattern, forbidenSymbols) {
  if (!data.Password) {
    return {
      error: true,
      name: 'Password',
      message: errorMessages().required,
    };
  } else if (data.Password.length > 10) {
    return {
      error: true,
      name: 'Password',
      message: errorMessages('Password', 10).maxLength,
    };
  } else if (!pattern.test(data.Password)) {
    return {
      error: true,
      name: 'Password',
      message: errorMessages().passwordPattern,
    };
  } else if (forbidenSymbols.test(data.Password)) {
    return {
      error: true,
      name: 'Password',
      message: errorMessages().forbidenSymbols,
    };
  } else {
    return null;
  }
}

export const searchValidation = (data, forbidenSymbols) => {
  if (data.length > 100) {
    return {
      error: true,
      name: 'Search',
      message: errorMessages('Search', 100).maxLength,
    };
  } else if (forbidenSymbols.test(data)) {
    return {
      error: true,
      name: 'Search',
      message: errorMessages().forbidenSymbols,
    };
  } else {
    return null;
  }
};

//
export const signInErrorHandler = data => {
  const emailPattern = /^.+@.+\.com|ua|ru$/g;
  const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)\S+$/g;
  const forbidenSymbols = /[<~>]/;

  return (
    emailValidation(data, emailPattern, forbidenSymbols) || passwordValidation(data, passwordPattern, forbidenSymbols)
  );
};

export const forgotPasswordErrorHandler = data => {
  const emailPattern = /^.+@.+\.com|ua|ru$/g;
  const forbidenSymbols = /[<~>]/;

  return emailValidation(data, emailPattern, forbidenSymbols);
};

export const searchErrorHandler = data => {
  const forbidenSymbols = /[<~>]/;

  return searchValidation(data, forbidenSymbols);
};
