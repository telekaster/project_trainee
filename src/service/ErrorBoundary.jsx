import React, { Component } from 'react';
import ErrorMessage from '../components/ErrorMessage';

export default class ErrorBoundary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      errorMessage: '',
    };
  }

  static getDerivedStateFromError(error) {
    return {
      hasError: true,
      errorMessage: error.message,
    };
  }

  render() {
    const { children } = this.props;
    const { errorMessage } = this.state;

    const cancelError = () => {
      this.state.hasError = false;
      this.state.errorMessage = '';
    };

    return this.state.hasError ? <ErrorMessage message={errorMessage} handler={cancelError} /> : children;
  }
}
