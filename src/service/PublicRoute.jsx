import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

export default function PublicRoute({ children, redirectTo = '/' }) {
  const isLoggedIn = useSelector(state => {
    return state.auth.loggedIn;
  });

  return isLoggedIn ? <Navigate to={redirectTo} /> : children;
}
