import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

export default function PrivateRoute({ children, redirectTo = '/auth' }) {
  const isLoggedIn = useSelector(state => {
    return state.auth.loggedIn;
  });
  const token = localStorage.getItem('AUTH_TOKEN');

  return token || isLoggedIn ? children : <Navigate to={redirectTo} />;
}
