export const ROUTES = {
  AUTH: '/auth',
  FORGOT_PASS: '/forgot-pass',
  MAIN: '/',
  ARCHIVE: '/archive',
  COURSE: '/course/:id',
};
