import React from 'react';
import { StyledHeader } from './StyledHeader';
import { Container } from '../../styles/Container';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { logoutAction } from '../../pages/Auth/actions/logout';
import { ROUTES } from '../../constants';
import Logo from './svg/Logo';
import LogoutIcon from './svg/LogoutIcon';

export default function Header() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const path = useLocation().pathname;

  const logoutHandler = () => {
    dispatch(logoutAction());
    localStorage.removeItem('AUTH_TOKEN');
    navigate(ROUTES.AUTH);
  };

  return (
    <StyledHeader>
      <Container>
        <div className="header-area">
          <Link to={ROUTES.MAIN} className="logo">
            <Logo />
          </Link>
          {path !== ROUTES.AUTH && path !== ROUTES.FORGOT_PASS && (
            <button className="logout-button" onClick={logoutHandler}>
              <LogoutIcon /> <span className="logout-button-text">Logout</span>
            </button>
          )}
        </div>
      </Container>
    </StyledHeader>
  );
}
