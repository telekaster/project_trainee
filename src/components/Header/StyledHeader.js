import styled from 'styled-components';
import { colors } from '../../styles/colors';
import { fonts } from '../../styles/fonts';

export const StyledHeader = styled.header`
  min-width: 1440px;
  width: 100%;
  background: ${colors.lightBlack};

  .header-area {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  .logo {
    width: 128px;
    display: block;
    padding-top: 24px;
    padding-bottom: 10px;
  }

  .logout-button {
    display: flex;
    align-items: center;

    cursor: pointer;
    border: none;
    background: inherit;

    font-family: ${fonts.mainFont};
    font-size: 16px;
    line-height: 1.25;
    color: ${colors.lightGrey};
  }
  .logout-button-text {
    display: block;
    margin-left: 5px;
  }
`;
