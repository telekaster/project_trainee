import styled from 'styled-components';

const StyledLoader = styled.div`
  .global {
    position: absolute;
    top: 50%;
    left: 50%;
  }
`;

export default StyledLoader;
