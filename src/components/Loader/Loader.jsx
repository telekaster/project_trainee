import React from 'react';
import StyledLoader from './StyledLoader';
import BounceLoader from 'react-spinners/ClipLoader';

export default function Loader({ global = false }) {
  return (
    <StyledLoader>
      <div className={global ? 'global' : ''}>
        <BounceLoader />
      </div>
    </StyledLoader>
  );
}
