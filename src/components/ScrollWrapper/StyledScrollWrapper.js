import styled from 'styled-components';

const StyledScrollWrapper = styled.div`
  height: 100vh;
  min-width: 1440px;
  width: 100%;

  overflow: scroll;
  scroll-behavior: auto;
`;

export default StyledScrollWrapper;
