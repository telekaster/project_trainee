import React from 'react';
import StyledScrollWrapper from './StyledScrollWrapper';

export default function ScrollWrapper({ children }) {
  return <StyledScrollWrapper>{children}</StyledScrollWrapper>;
}
