import React from 'react';
import HideIcon from './svg/HideIcon';
import ShowIcon from './svg/ShowIcon';
import uniqid from 'uniqid';
import { useState } from 'react';
import { InputArea } from './StyledInput';

export default function Input({ inputType, label, register, error }) {
  const [visible, setVisible] = useState(false);
  const id = uniqid();

  const visibility = () => {
    setVisible(!visible);
  };

  return (
    <InputArea>
      {label && (
        <>
          <label htmlFor={id} className="input-title">
            {label}
          </label>
          <input id={id} type={(visible && 'text') || inputType} className="input" {...register(label)} />
        </>
      )}

      {label && inputType === 'password' && (
        <button type="button" className="button-show" onClick={visibility}>
          {!visible ? <ShowIcon /> : <HideIcon />}
        </button>
      )}

      {error[label] && <p className="error-message">{error[label].message}</p>}
    </InputArea>
  );
}
