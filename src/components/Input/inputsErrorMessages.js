export default function inputsErrorMessages(label, maxLength) {
  return {
    required: 'This field is required',
    maxLength: `${label} should be max ${maxLength} symbols`,
    emailPattern: 'Email should contains @ sign and domain name .com/.ru/.ua',
    passwordPattern: 'Password should contains lower/upper case letter and any number',
    forbidenSymbols: 'Forbiden symbols: <, >, ~',
  };
}
