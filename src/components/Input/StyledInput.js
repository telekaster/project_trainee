import styled from 'styled-components';
import { fonts } from '../../styles/fonts';
import { colors } from '../../styles/colors';

export const InputArea = styled.div`
  margin-bottom: 40px;
  position: relative;

  .input-title {
    display: block;
    margin-bottom: 8px;
    color: ${colors.grey};
    font-family: ${fonts.mainFont};
    font-size: 18px;
    line-height: 1.11;
  }

  .input {
    min-width: 503px;
    padding-bottom: 8px;

    background: inherit;
    border: none;
    border-bottom: 2px solid ${colors.transparentGrey};
    font-family: ${fonts.mainFont};
    font-size: 24px;
    line-height: 1.25;
    color: ${colors.white};
  }

  .error-message {
    font-family: ${fonts.mainFont};
    font-size: 18px;
    line-height: 1.11;
    color: red;

    position: absolute;
    transform: translateY(5px);
  }
  .password {
    letter-spacing: 16px;
  }

  .button-show {
    position: absolute;
    transform: translate(-28px, 1px);

    cursor: pointer;
    background: inherit;
    border: none;
  }
`;
