import React from 'react';

export default function Arrow() {
  return (
    <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0 7.966h15.643v2.068H0V7.966Z" fill="#1F1F1F" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.5 0 18 9l-8.5 9-1.38-1.462L15.238 9l-7.12-7.538L9.5 0Z"
        fill="#1F1F1F"
      />
    </svg>
  );
}
