import styled from 'styled-components';
import { fonts } from '../../styles/fonts';
import { colors } from '../../styles/colors';

export const StyledButton = styled.button`
  width: 503px;
  height: 56px;
  padding-left: 16px;
  padding-right: 19px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  background: ${colors.white};
  border: none;
  cursor: pointer;

  font-family: ${fonts.mainFont};
  font-size: 18px;
  line-height: 1.33;
  color: ${colors.black};
`;
