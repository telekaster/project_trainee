import React from 'react';
import { StyledButton } from './StyledButton';

export default function Button({ text, ButtonIcon, handler }) {
  return (
    <StyledButton onClick={handler}>
      {text}
      {ButtonIcon}
    </StyledButton>
  );
}
