import StyledErrorMessage from './StyledErrorMessage';
import Button from '../Button';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../../constants';
import { Container } from '../../styles/Container';

export default function ErrorMessage({ title = 'Something went wrong:', message, handler }) {
  const navigate = useNavigate();

  const goHome = () => {
    navigate(ROUTES.MAIN);

    if (handler) {
      handler();
    }
  };

  return (
    <StyledErrorMessage className="error-boundary-area">
      <div className="error-header"></div>
      <Container>
        <div className="error-boundary">
          <h2 className="error-title">
            {title}
            <span className="error-text">{message}</span>
          </h2>
          <Button text={'Lets go to the main page'} className="error-button" handler={goHome} />
        </div>
      </Container>
    </StyledErrorMessage>
  );
}
