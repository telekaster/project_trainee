import styled from 'styled-components';
import { colors } from '../../styles/colors';

const StyledErrorMessage = styled.div`
  background: #b0b0b0;
  height: 100vh;
  min-width: 1440px;
  width: 100%;

  .error-header {
    height: 213px;
    width: 100%;
    background-color: ${colors.lightBlack};
  }
  .error-boundary {
    padding-top: 50px;
    color: #1f1f1f;
  }

  .error-title {
    margin-bottom: 50px;
  }

  .error-text {
    color: red;
  }
`;

export default StyledErrorMessage;
